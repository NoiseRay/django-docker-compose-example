from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.http import JsonResponse
from django.urls import path
from django.http import HttpResponse

def json(request):
    return JsonResponse({'hello': 'world'})

def hello(request):
    return HttpResponse('Gracias por escucharme!')

urlpatterns = [
    path('json', json, name='home'),
    path('', hello, name='home'),
    path('admin/', admin.site.urls),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
